/* Test kernel. 
 * Performs copy of a frame described as an array of bytes (Y*X*RGB)
 */
#include "dev/mango_hn.h"
#include "dev/debug.h"


#pragma mango_kernel
void copy_kernel_function(uint8_t *out, uint8_t *in, int X, int Y){
	for(int x=0; x<X; x++)
		for(int y=0; y<Y; y++)
			for(int c=0; c<3; c++)
				out[y*X*3+x*3+c] = in[y*X*3+x*3+c];
}

