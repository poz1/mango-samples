/* 
 *
 * Test application. This application
 * will launch a kernel in PEAK. The kernel will just
 * compute a matrix multiplication. The size of the matrix 
 * and the matrices pointers will be passed as parameters
 *
 * After executing the kernel, the application shows
 * the resulting output matrix
 *
 * This application demonstrates read & write communication between
 * kernel and host application.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "host/mango.h"

#define KID 1
#define BID 1

int main(int argc, char**argv) {

  uint32_t n=3;

  /* initialization of the mango context */
  mango_init("app", "generic");

#ifdef GNEMU
  char kernel_file[] = "/opt/mango/usr/local/share/sync/sync_dev";
#else
  char kernel_file[] = "/opt/mango/usr/local/share/sync/memory.data.fpga.datafile";
#endif


  kernelfunction *k = mango_kernelfunction_init();
#ifdef GNEMU
  mango_load_kernel(kernel_file, k, GN, BINARY);
#else
  mango_load_kernel(kernel_file, k, PEAK, BINARY);
#endif
  mango_kernel_t k1 = mango_register_kernel(KID, k, 0, 1, BID);  
  mango_buffer_t b1 = mango_register_memory(BID, sizeof(uint64_t), BUFFER, 1, 0, k1);  
  mango_event_t e1 = mango_register_event(1, 1, k1, k1);

  /* Registration of task graph */
  mango_task_graph_t *tg = mango_task_graph_create(1, 1, 1, k1, b1, e1);

  /* resource allocation */
  mango_resource_allocation(tg);

  /* Execution preparation */
  mango_arg_t *arg1 = mango_arg( k1, &n, sizeof(uint32_t), SCALAR );
  mango_arg_t *arg2 = mango_arg( k1, &e1, sizeof(mango_event_t), EVENT );
  mango_arg_t *arg3 = mango_arg( k1, &b1, sizeof(uint64_t), BUFFER );
  mango_args_t *args=mango_set_args(k1, 3, arg1, arg2, arg3);

  /* spawn kernel */
  mango_event_t ev = mango_start_kernel(k1, args, NULL);

  for (int i=0; i < n; i++) {
	printf("HOST: Waiting %d...\n", 1);
	mango_wait_state(e1, 1);
	printf("HOST: Signaling %d...\n", 2);
	mango_write_synchronization(e1, 2);
  }

  /* wait for kernel completion */
  mango_wait(ev);

  /* shut down the mango infrastructure */
	mango_resource_deallocation(tg);
	mango_task_graph_destroy_all(tg);
	mango_release();

  return 0;	
}	


