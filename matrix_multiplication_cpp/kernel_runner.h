#ifndef KERNEL_RUNNER_H
#define KERNEL_RUNNER_H
#include <host/mango.h>


class KernelRunner
{
private:
    mango::BBQContext *mango_rt;
    mango::KernelArguments *argsKERNEL;
    mango::TaskGraph *tg;
    enum { HOST=0, KERNEL };
    enum { B1=1, B2, B3 };
    int  Rows;
    int  Columns;
    
public:
    KernelRunner(int Rows, int Columns);

    ~KernelRunner();

    void run_kernel(int* A, int*  B, int*  C);
};

#endif
