#include <stdio.h>
#include "kernel_runner.h"


//matrix size
const int Rows = 9;
const int Column = 9;

int*  A = static_cast<int *>(malloc(Rows * Column * sizeof(int)));
int*  B = static_cast<int *>(malloc(Rows * Column * sizeof(int)));
int*  C = static_cast<int *>(malloc(Rows * Column * sizeof(int)));

void init_matrix(int *matrix, int rows, int cols)
{
    for (int r=0;r<rows;r++) {
        for (int c=0;c<cols;c++) {
            matrix[r*cols+c] = random() % 100;
        }
    }
}

int main()
{
    printf("Starting setup\n");
    auto kr=KernelRunner(Rows,Column);

    init_matrix(A, Rows, Column);
    init_matrix(B, Rows, Column);

    printf("Running kernel\n");
    kr.run_kernel(A, B, C);
   
    printf("\n\nMatrix C (Results)\n");
    int i;
    for(i = 0; i < 9; i++)
    {
        printf("%d ", C[i]);
        if(((i + 1) % 3) == 0)
            printf("\n");
    }
    printf("\n");
}
