#include "kernel_runner.h"
#include "host/logger.h"

#include <string>

KernelRunner::KernelRunner(int rows, int columns)
{
    this -> Rows = rows;
    this -> Columns = columns;

    // Initialization
    mango::mango_init_logger();
    // Initialization
    mango_rt = new mango::BBQContext("gif_animation", "gif_animation");

    const char *mango_root = getenv("MANGO_ROOT");
    std::string kf_path;

    if (mango_root == NULL) {
        kf_path = "/opt/mango";
    } else {
        kf_path = mango_root;
    }


#ifdef GNEMU
    char scale_kernel_file[] = "/usr/local/share/matrix_multiplication/matrix_multiplication_dev";
#else
    char scale_kernel_file[] = "/usr/local/share/matrix_multiplication/memory.data.fpga.datafile";
#endif


#ifdef GNEMU
    auto kf = new mango::KernelFunction();
    kf->load(kf_path + scale_kernel_file, mango::UnitType::GN, mango::FileType::BINARY);
#else
    auto kf = new mango::KernelFunction();
    kf->load(kf_path + scale_kernel_file, mango::UnitType::PEAK, mango::FileType::BINARY);
#endif

    // Registration of task graph
    auto k  = mango_rt->register_kernel(KERNEL, kf, {B1, B2}, {B3});

    auto b1 = mango_rt->register_buffer(B1, Rows*Columns*sizeof(int), {}, {KERNEL}, mango::BufferType::FIFO );
    auto b2 = mango_rt->register_buffer(B2, Rows*Columns*sizeof(int), {}, {KERNEL}, mango::BufferType::FIFO );
    auto b3 = mango_rt->register_buffer(B3, Rows*Columns*sizeof(int), {KERNEL}, {}, mango::BufferType::FIFO );

    tg = new mango::TaskGraph({ k }, { b1, b2, b3 });

    // Resource Allocation
    mango_rt->resource_allocation(*tg);

    // Execution setup
    auto argB1 = new mango::BufferArg( b1 );
    auto argB2 = new mango::BufferArg( b2 );
    auto argB3 = new mango::BufferArg( b3 );

    auto argSX = new mango::ScalarArg<int>( 3 );
    auto argSY = new mango::ScalarArg<int>( 3 );

    auto argE1 = new mango::EventArg( b3->get_event() );

    argsKERNEL = new mango::KernelArguments({ argB1, argB2,argB3, argSX, argSY, argE1 }, k);
}

KernelRunner::~KernelRunner()
{
    // Deallocation and teardown
    mango_rt->resource_deallocation(*tg);
}

void KernelRunner::run_kernel(int* A, int*  B, int*  C  )
{
    auto b1 = mango_rt->get_buffer(B1);
    auto b2 = mango_rt->get_buffer(B2);
    auto b3 = mango_rt->get_buffer(B3);

    auto k  = mango_rt->get_kernel(KERNEL);

    b1->write(A, Rows*Columns*sizeof(int));
    b2->write(B, Rows*Columns*sizeof(int));
    
    b3->read(C, Rows*Columns*sizeof(int));

    auto e3=mango_rt->start_kernel(k, *argsKERNEL);

    e3->wait();
}
